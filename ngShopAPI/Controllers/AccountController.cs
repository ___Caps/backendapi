﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Bogus;
using DAL;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using ngShopAPI.Models;

namespace ngShopApi.Controllers
{
    public class Credentials
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
    }
    [Produces("application/json")]
    [Route("api/account")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private UserManager<DbUser> _userManager;
        private SignInManager<DbUser> _signInManager;
        private RoleManager<IdentityRole> _roleManager;
        private EFContext _context;
        public AccountController(UserManager<DbUser> userManager, RoleManager<IdentityRole> roleManager,
            SignInManager<DbUser> signInManager, EFContext context)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _roleManager = roleManager;
            _context = context;
        }
        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody]Credentials credentials)
        {
            var Iuser = new DbUser
            {
                UserName = credentials.Email,
                Email = credentials.Email 
            };
            var user = new UserInfo
            {
                Id = Iuser.Id,
                Name = credentials.Name,
                Email = credentials.Email,
                Password = credentials.Password,
                RegisterTime = DateTime.UtcNow,
                RoleId = 2,
                userDetails = "new User"
            };
            if (!_roleManager.Equals("User"))
            {
                IdentityResult role = await _roleManager.CreateAsync(new IdentityRole("User"));
            }
            var result = await _userManager.CreateAsync(Iuser, credentials.Password);
           
            _context.UserInfo.Add(user);
            if (result.Succeeded)
            {
                await _userManager.AddToRoleAsync(Iuser, "User");
                _context.SaveChanges();
                await _signInManager.SignInAsync(Iuser, isPersistent: false);
                return Ok(CreateToken(Iuser, "User"));
            }
            return BadRequest(result.Errors);
        }


        [HttpPost("login")]
        public async Task<IActionResult> Login([FromBody]Credentials credentials)
        {
            var result = await _signInManager
                .PasswordSignInAsync(credentials.Email, credentials.Password, false, false);
            if (!result.Succeeded)
                return Unauthorized();

            var user = await _userManager.FindByEmailAsync(credentials.Email);
            await _signInManager.SignInAsync(user, isPersistent: false);
            var roles = await _userManager.GetRolesAsync(user);
            return Ok(CreateToken(user, roles.First()));
        }
        
        string CreateToken(DbUser user, string role)
        {
            var claims = new Claim[]
            {
                new Claim("id", user.Id),
                new Claim("name", user.UserName),
                new Claim("role", role)
            };
            var signingKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("this is secret phrase777"));
            var signingCredentials = new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256);

            var jwt = new JwtSecurityToken(signingCredentials: signingCredentials, claims: claims);
            return new JwtSecurityTokenHandler().WriteToken(jwt);
        }


        [HttpGet("role")]
        public IActionResult GetRole()
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var userRole = User.FindFirst(ClaimTypes.Role).Value;

            if (userRole == null)
            {
                return NotFound();
            }
            return Ok(userRole);
        }


        [HttpGet("name/{email}")]
        public async Task<IActionResult> GetUserName([FromRoute] string email)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = await _context.UserInfo.SingleOrDefaultAsync(m => m.Email == email);

            if (user == null)
            {
                return NotFound();
            }
            return Ok(user.Name);
        }

        //[HttpPost("test")]
        //public async Task<IActionResult> fake()
        //{
        //    for (int i = 0; i < 100; i++)
        //    {
        //        var usera = new Faker<UserInfo>()
        //                .RuleFor(bp => bp.Name, f => f.Person.UserName)
        //                .RuleFor(bp => bp.Email, (f, u) => f.Internet.Email(u.Name))
        //                .RuleFor(bp => bp.RegisterTime, f => f.Date.Past())
        //                .RuleFor(bp => bp.Password, f => "123456")
        //                    .RuleFor(bp => bp.userDetails, f => "fake");
        //        var user = usera.Generate();
        //        var Iuser = new DbUser
        //        {
        //            UserName = user.Email,
        //            Email = user.Email
        //        };
        //        var useri = new UserInfo
        //        {
        //            Id = Iuser.Id,
        //            Name = user.Name,
        //            Email = user.Email,
        //            Password = user.Password,
        //            RegisterTime = DateTime.UtcNow,
        //            RoleId = 2,
        //            userDetails = user.userDetails
        //        };

        //        var result = await _userManager.CreateAsync(Iuser, user.Password);

        //        _context.UserInfo.Add(useri);
        //        if (result.Succeeded)
        //        {
        //            await _userManager.AddToRoleAsync(Iuser, "User");
        //            _context.SaveChanges();
                   
        //        }
        //    }
        //    return Ok();
        //}
    }
}