﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using DAL;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ngShopAPI.Models;

namespace ngShopApi.Controllers
{
    public class AddOrderModel
    {
        public string userId { get; set; }
        public int productId { get; set; }
    }
    public class PutCount
    {
        public int productId { get; set; }
        public int count { get; set; }
    }

    public class Putproduct
    {
        public int id { get; set;}
        public int phases { get; set; }
        public float Power { get; set; }
        public float Money { get; set; }

        public string Description { get; set; }
    }
    [Produces("application/json")]
    [Route("api/user")]
    [ApiController]
    public class UserController : ControllerBase
    {

        public EFContext _context;
        public UserController( EFContext context)
        {
            _context = context;
        }

        //work
        [Authorize(Roles = "User")]
        [HttpGet("order")]
        public async Task<IActionResult> GetOrders()
        {
            var userId = User.FindFirst("id").Value;
            var userName = User.FindFirst("name").Value;
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var list = await _context.OrderProducts
                .Where(u => u.Order.UserId == userId)
                .Select(p => new
                {
                    Id = p.FrequencyConverter.Id,
                    Image = p.FrequencyConverter.Series.ImagePath,
                    Series = p.FrequencyConverter.Series.SeriesName,
                    Power = p.FrequencyConverter.Power.CountPower,
                    Phases = p.FrequencyConverter.CountPhases.PhasesCount,
                    Money = p.FrequencyConverter.MoneyCost,
                    Count = p.Count
                }).ToListAsync();

            return Ok(list);
        }

        [Authorize(Roles = "User")]
        [HttpPost("orderProduct")]
        public IActionResult OrderProduct([FromBody] int productId)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var userId = User.FindFirst("id").Value;
            var order = new DAL.Order
            {
                UserId = userId
            };
            _context.Orders.Add(order);
            _context.SaveChanges();
            var exist = _context.OrderProducts.Any(w=>w.FrequencyConverterId == productId && w.Order.UserId == userId);
            if (exist)
            {
                var product = _context.OrderProducts.Where(w => w.FrequencyConverterId == productId && w.Order.UserId == userId).FirstOrDefault();
                product.Count++;
                _context.SaveChanges();
                return new NoContentResult();
            }
            else
            {
                var oproduct = new DAL.OrderProduct
                {
                    FrequencyConverterId = productId,
                    OrderId = order.Id,
                    Count = 1
                };
                _context.OrderProducts.Add(oproduct);
                _context.SaveChanges();
                if (oproduct == null)
                {
                    return NotFound();
                }
                return Ok();
            }
           
        }

       
        [Authorize(Roles = "User")]
        [HttpPut("product/count")]
        public IActionResult SetCount([FromBody] PutCount bodyObj)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var userId = User.FindFirst("id").Value;
            
            var product = _context.OrderProducts.Where(w=>w.FrequencyConverterId == bodyObj.productId && w.Order.UserId == userId).FirstOrDefault();

            if (product != null)
            {
                product.Count = bodyObj.count;
                _context.SaveChanges();
                return new NoContentResult();
            }
            else
            {
                return NotFound();
            }
            
        }
        [Authorize(Roles = "User")]
        [HttpDelete("order")]
        public IActionResult DeleteOrder([FromBody] PutCount bodyObj)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var userId = User.FindFirst("id").Value;

            var product = _context.OrderProducts.Where(w => w.FrequencyConverterId == bodyObj.productId && w.Order.UserId == userId).FirstOrDefault();

            if (product != null)
            {
                _context.OrderProducts.Remove(product);
                Order o = new Order { Id = product.OrderId};
                _context.Orders.Attach(o);
                _context.Orders.Remove(o);
                _context.SaveChanges();
                return Ok();
            }
            else
            {
                return NotFound();
            }

        }
    }
}