﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ngShopAPI.Models;

namespace ngShopApi.Controllers
{
    public class ListProductsModel
    {
        public int Id { get; set; }
        public string LSeries { get; set; }
        public float LPower { get; set; }
        public int LPhases { get; set; }
        public float LMoney { get; set; }

        public string LDescription { get; set; }
    }

    public class ListUserModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Role { get; set; }
    }
    [ApiController]
    [Produces("application/json")]
    [Route("api/admin")]
    public class AdminController : ControllerBase
    {
        private EFContext _context;
        public AdminController(EFContext context)
        {
            _context = context;
        }

        [HttpGet("{page?}")]
        public IActionResult Index(int page = 1)
        {
            int pageSize = 10;
            //var users = _context.UserInfo.Include(u => u.Role).ToList();
           
            var list= _context.UserInfo.Include(u => u.Role).Skip((page - 1) * pageSize).Take(pageSize).Select(p => new ListUserModel
            {
                Id = p.Id,
                Name = p.Name,
                Email = p.Email,
                Role = p.Role.Name
            }).ToList();

            return Ok(list);
        }
        [HttpGet("index")]
        public int LastIndex()
        {
            return _context.UserInfo.Count();
        }




        [HttpGet("products/{page?}")]
        public IActionResult Products(int page = 1)
        {
            int pageSize = 10;
            //var products = _context.FrequencyConverters.ToList();

            var list = _context.FrequencyConverters.Include(p => p.Series).Include(p => p.Power).Include(pc => pc.CountPhases).Skip((page - 1) * pageSize).Take(pageSize).Select(p => new ListProductsModel
            {
                Id = p.Id,
                LSeries = p.Series.SeriesName,
                LPower = p.Power.CountPower,
                LPhases = p.CountPhases.PhasesCount,
                LMoney = p.MoneyCost
            }).ToList();

            return Ok(list);
        }
        [HttpGet("product_index")]
        public int LastProductIndex()
        {
            return _context.FrequencyConverters.Count();
        }


        [HttpGet("user/{id}")]
        public async Task<IActionResult> GetUser([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = await _context.UserInfo.SingleOrDefaultAsync(m => m.Id == id);

            if (user == null)
            {
                return NotFound();
            }

            return Ok(user);
        }


        [Authorize(Roles = "Admin")]
        [HttpPut("product/update")]
        public IActionResult Update([FromBody] Putproduct bodyObj)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var userId = User.FindFirst("id").Value;

            var product = _context.FrequencyConverters.Where(w => w.Id == bodyObj.id).FirstOrDefault();

            if (product != null)
            {
                product.Power.CountPower = bodyObj.Power;
                product.CountPhases.PhasesCount = bodyObj.phases;
                product.Description.Descriptions = bodyObj.Description;
                product.MoneyCost = bodyObj.Money;
                _context.SaveChanges();
                return new NoContentResult();
            }
            else
            {
                return NotFound();
            }

        }

        [HttpGet("product/{id}")]
        public async Task<IActionResult> GetProduct([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            ListProductsModel model = new ListProductsModel();
            var product = await _context.FrequencyConverters.Include(s=>s.Series).Include(p => p.Power).Include(pc => pc.CountPhases).Include(pc => pc.Description).SingleOrDefaultAsync(m => m.Id == id);
            model.LSeries = product.Series.SeriesName;
            model.LPhases = product.CountPhases.PhasesCount;
            model.LPower = product.Power.CountPower;
            model.LMoney = product.MoneyCost;
            model.LDescription = product.Description.Descriptions;
            if (product == null)
            {
                return NotFound();
            }

            return Ok(model);
        }


        [HttpDelete("product/delete/{id}")]
        public async Task<IActionResult> DeleteProduct([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var Fconveter = await _context.FrequencyConverters.SingleOrDefaultAsync(m => m.Id == id);
            if (Fconveter == null)
            {
                return NotFound();
            }

            _context.FrequencyConverters.Remove(Fconveter);
            await _context.SaveChangesAsync();

            return Ok(Fconveter);
        }
        [HttpDelete("user/delete/{id}")]
        public async Task<IActionResult> DeleteUser([FromRoute] string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = await _context.UserInfo.SingleOrDefaultAsync(m => m.Id == id);
            var Iuser = await _context.Users.SingleOrDefaultAsync(m => m.Id == id);
            var rIuser = await _context.UserRoles.SingleOrDefaultAsync(m => m.UserId == id);
            if (user == null)
            {
                return NotFound();
            }

            _context.UserInfo.Remove(user);
            _context.Users.Remove(Iuser);
            _context.UserRoles.Remove(rIuser);
            await _context.SaveChangesAsync();

            return Ok(user);
        }

        //[HttpGet("name/search/{req}")]
        //public IActionResult Search_User([FromRoute] string req)
        //{
        //    if (!ModelState.IsValid)
        //    {
        //        return BadRequest(ModelState);
        //    }
        //    List<UserInfo> response = new List<UserInfo>();
        //    foreach (var item in _context.UserInfo.ToList())
        //    {
        //        if (item.Name.ToLower().Contains(req.ToLower()))
        //        {
        //            response.Add(item);
        //        }
        //    }
        //    return Ok(response);
        //}


        public class FilterProductModel
        {
            public List<ProductViewModel> Products { get; set; }
            public int Count { get; set; }
        }
        public class FilterUserModel
        {
            public List<UserViewModel> Users { get; set; }
            public int Count { get; set; }
        }


        [HttpGet("user/search")]
        public IActionResult Search_User([FromQuery] string req, int? page)
        {
            FilterUserModel model = new FilterUserModel();
            int count = _context.UserInfo
              .Where(u => u.Name.ToLower()
              .Contains(req.ToLower())).Count();
            model.Count = count;
            if (page == null)
            {
                model.Users = SearchUserHelper(req);
            }
            else
            {
                model.Users = SearchUserHelper(req, page.Value);
            }

            return Ok(model);
        }

        private List<UserViewModel> SearchUserHelper(string request, int page = 1)
        {
            int pageSize = 10;
            var query = _context.UserInfo
                .Where(u => u.Name.ToLower()
                .Contains(request.ToLower()));

            var listUserSearch = query.Skip((page - 1) * pageSize).Take(pageSize).Select(p => new UserViewModel
            {
                Id = p.Id,
                Name = p.Name,
                Email = p.Email,
                Role = p.Role.Name
            }).ToList();
            return listUserSearch;
        }




        [HttpGet("product/search")]
        public IActionResult Search_Product([FromQuery] string req, int ?page )
        {
            FilterProductModel model = new FilterProductModel();
            int count = _context.FrequencyConverters
              .Where(u => u.Series.SeriesName.ToLower()
              .Contains(req.ToLower())).Count();
            model.Count = count;
            if (page == null)
            {
                model.Products = SearchProductHelper(req);
            }
            else
            {
                model.Products = SearchProductHelper(req, page.Value);
            }

            return Ok(model);
        }

        private List<ProductViewModel> SearchProductHelper(string request, int page = 1)
        {
            int pageSize = 10;
            var query = _context.FrequencyConverters
                .Where(u => u.Series.SeriesName.ToLower()
                .Contains(request.ToLower()));

            var listProductSearch = query.Skip((page - 1) * pageSize).Take(pageSize).Select(p => new ProductViewModel
            {
                Id = p.Id,
                LSeries = p.Series.SeriesName,
                LPower = p.Power.CountPower,
                LPhases = p.CountPhases.PhasesCount,
                LMoney = p.MoneyCost
            }).ToList();
            return listProductSearch;
        }
        
    }
}