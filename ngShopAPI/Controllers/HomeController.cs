﻿using DAL;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ngShopAPI.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ngShopApi.Controllers
{
    public class ListProductsAllModel
    {
        public int Id { get; set; }
        public string LSeries { get; set; }
        public float LPower { get; set; }
        public int LPhases { get; set; }
        public float LMoney { get; set; }
        public string LDescription { get; set; }
        public string LImagePath { get; set; }
    }



    [ApiController]
    [Produces("application/json")]
    [Route("api/home")]
    public class HomeController : ControllerBase
    {
        private readonly EFContext _context;
        public HomeController(EFContext context)
        {
            _context = context;
        }


        public ActionResult Index()
        {
            HomeViewModel model = new HomeViewModel();
            model.Filters = GetListFilters();
            int[] valuesId = new int[0];
            model.Products = GetProductsByFilter(valuesId, model.Filters);
            return Ok(model);
        }


        [HttpGet("filter")]
        public ActionResult<HomeViewModel> FilterProduct([FromQuery] string[] id, int? page)
        {
            HomeViewModel model = new HomeViewModel();
            model.Filters = GetListFilters();
            int[] valuesId = new int[0];
            if (id != null)
            {
                valuesId = id.Select(v => int.Parse(v)).ToArray();
            }

            if (page == null)
            {
                model.Products = GetProductsByFilter(valuesId, model.Filters);
            }
            else
            {
                model.Products = GetProductsByFilter(valuesId, model.Filters, page.Value);
            }
            return model;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> GetFrequrencyConverter([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var Fconverter = await _context.FrequencyConverters.SingleOrDefaultAsync(m => m.Id == id);

            if (Fconverter == null)
            {
                return NotFound();
            }

            return Ok(Fconverter);
        }


        [HttpGet("search")]
        public async Task<IActionResult> SearchProductHelper([FromQuery] string request)
        {

            var query = _context.FrequencyConverters
                .Where(u => u.Series.SeriesName.ToLower()
                .Contains(request.ToLower()));

            var listProductSearch = await query.Select(p => new ProductViewModel
            {
                Id = p.Id,
                LSeries = p.Series.SeriesName,
                LPower = p.Power.CountPower,
                LPhases = p.CountPhases.PhasesCount,
                LMoney = p.MoneyCost,
                LImagePath = p.Series.ImagePath
            }).ToListAsync();
            return Ok(listProductSearch);
        }


        private bool ConvertorExists(int id)
        {
            return _context.FrequencyConverters.Any(e => e.Id == id);
        }

        private List<FNameViewModel> GetListFilters()
        {
            var query = from f in _context.VFilterNameGroups.AsQueryable()
                        where f.FilterValueId != null
                        select new
                        {
                            FNameId = f.FilterNameId,
                            FName = f.FilterName,
                            FValueId = f.FilterValueId,
                            FValue = f.FilterValue
                        };
            var groupNames = from f in query
                             group f by new
                             {
                                 Id = f.FNameId,
                                 Name = f.FName
                             } into g
                             orderby g.Key.Name
                             select g;
            List<FNameViewModel> listGroupFilters = new List<FNameViewModel>();
            foreach (var filterName in groupNames)
            {
                FNameViewModel fName = new FNameViewModel
                {
                    Id = filterName.Key.Id,
                    Name = filterName.Key.Name
                };

                fName.Children = (from v in filterName
                                  group v by new FValueViewModel
                                  {
                                      Id = v.FValueId,
                                      Name = v.FValue
                                  } into g
                                  select g.Key).ToList();

                listGroupFilters.Add(fName);
            }
            return listGroupFilters;
        }

        private List<ProductViewModel> GetProductsByFilter(int[] values, List<FNameViewModel> filtersList, int page = 1)
        {
            int pageSize = 15;
            int[] filterValueSearchList = values;
            var query = _context
                .FrequencyConverters
                .Include(f => f.Filtres)
                .AsQueryable();

            foreach (var fName in filtersList)
            {
                int count = 0; //Кількість співпадінь у даній групі фільрів
                var predicate = PredicateBuilder.False<FrequencyConverter>();
                foreach (var fValue in fName.Children)
                {
                    for (int i = 0; i < filterValueSearchList.Length; i++)
                    {
                        var idV = fValue.Id;
                        if (filterValueSearchList[i] == idV)
                        {
                            predicate = predicate
                                .Or(p => p.Filtres
                                    .Any(f => f.FilterValueId == idV));
                            count++;
                        }
                    }
                }
                if (count != 0)
                {
                    query = query.Where(predicate);
                }
            }

            var listProductSearch = query.Skip((page - 1) * pageSize).Take(pageSize).Select(p => new ProductViewModel
            {
                Id = p.Id,
                LImagePath = p.Series.ImagePath,
                LSeries = p.Series.SeriesName,
                LPower = p.Power.CountPower,
                LPhases = p.CountPhases.PhasesCount,
                LMoney = p.MoneyCost
            }).ToList();


            return listProductSearch;
        }

    }
}