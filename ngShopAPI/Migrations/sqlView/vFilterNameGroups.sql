IF NOT EXISTS (SELECT * FROM sys.views WHERE object_id = OBJECT_ID(N'[dbo].[vFilterNameGroups]'))
EXEC dbo.sp_executesql @statement = N'
CREATE VIEW [dbo].[vFilterNameGroups]
AS
SELECT        NEWID() AS Id, fn.Id AS FilterNameId, fn.Name AS FilterName, fv.Id AS FilterValueId, fv.Name AS FilterValue
FROM            dbo.FilterNames AS fn LEFT OUTER JOIN
                         dbo.FilterNameGroups AS fng ON fn.Id = fng.FilterNameId LEFT OUTER JOIN
                         dbo.FilterValues AS fv ON fv.Id = fng.FilterValueId'