﻿using DAL;
using ngShopAPI.Models;

namespace myShopAPI
{
    public static class SampleData
    {
        public static void Initialize(EFContext context)
        {
            if (false)
            {
                //string codeBase = Assembly.GetExecutingAssembly().CodeBase;
                //UriBuilder uri = new UriBuilder(codeBase);
                //string path = Uri.UnescapeDataString(uri.Path);

                //string baseDir = Path.GetDirectoryName(path) + "\\Migrations\\sqlView\\vFilterNameGroups.sql";
                //context.Database.ExecuteSqlCommand(File.ReadAllText(baseDir));

                #region Create_Products
                //context.Series.AddRange(
                //    new Series
                //    {
                //        SeriesName = "Starvert iE5",
                //        ImagePath = "https://res.cloudinary.com/ng-shop/image/upload/v1552142702/ng-Shop/ie5.jpg"
                //    },
                //    new Series
                //    {
                //        SeriesName = "Starvert iC5",
                //        ImagePath = "https://res.cloudinary.com/ng-shop/image/upload/v1552132420/ng-Shop/ic5.png"
                //    },
                //    new Series
                //    {
                //        SeriesName = "Starvert iG5A",
                //        ImagePath = "https://res.cloudinary.com/ng-shop/image/upload/v1552142702/ng-Shop/ig5.png"
                //    },
                //    new Series
                //    {
                //        SeriesName = "S100",
                //        ImagePath = ""
                //    },
                //    new Series
                //    {
                //        SeriesName = "Starvert iS7",
                //        ImagePath = ""
                //    },
                //    new Series
                //    {
                //        SeriesName = "Starvert iV5",
                //        ImagePath = ""
                //    },
                //    new Series
                //    {
                //        SeriesName = "Starvert iP5A",
                //        ImagePath = ""
                //    }, new Series
                //    {
                //        SeriesName = "H100",
                //        ImagePath = ""
                //    });
                //context.CountPhases.AddRange(
                //        new CountPhases
                //        {
                //            PhasesCount = 1
                //        },
                //        new CountPhases
                //        {
                //            PhasesCount = 3
                //        });

                //context.Description.AddRange(
                //       new Description
                //       {
                //           Descriptions = "test",
                //           NameSeries = "test"
                //       });

                //context.Power.AddRange(
                //       new Power
                //       {
                //           CountPower = 0.1F
                //       },
                //       new Power
                //       {
                //           CountPower = 0.2F
                //       },
                //        new Power
                //        {
                //            CountPower = 0.4F
                //        },
                //         new Power
                //         {
                //             CountPower = 0.75F
                //         },
                //          new Power
                //          {
                //              CountPower = 1.5F
                //          },
                //           new Power
                //           {
                //               CountPower = 2.2F
                //           },
                //            new Power
                //            {
                //                CountPower = 3.7F
                //            },
                //             new Power
                //             {
                //                 CountPower = 4F
                //             },
                //              new Power
                //              {
                //                  CountPower = 5.5F
                //              },
                //               new Power
                //               {
                //                   CountPower = 7.5F
                //               },
                //                new Power
                //                {
                //                    CountPower = 11F
                //                },
                //                 new Power
                //                 {
                //                     CountPower = 15F
                //                 }, new Power
                //                 {
                //                     CountPower = 18.5F
                //                 }, new Power
                //                 {
                //                     CountPower = 22
                //                 }, new Power
                //                 {
                //                     CountPower = 30
                //                 }, new Power
                //                 {
                //                     CountPower = 37
                //                 }, new Power
                //                 {
                //                     CountPower = 45
                //                 }, new Power
                //                 {
                //                     CountPower = 55
                //                 }, new Power
                //                 {
                //                     CountPower = 75
                //                 });


                //context.FrequencyConverters.AddRange(
                //new FrequencyConverter
                //{
                //    SeriesId = 1,
                //    PowerId = 3,
                //    PhasesId = 1,
                //    idDescriptions = 1,
                //    MoneyCost = 211.56F,
                //    Size = "68x128x85",
                //    Model = "INV,SV004IE5-1",
                //    Article = "6620000500",
                //    Amperage = 2.5F
                //},
                //new FrequencyConverter
                //{
                //    SeriesId = 1,
                //    PowerId = 1,
                //    PhasesId = 2,
                //    idDescriptions = 1,
                //    MoneyCost = 209.7F,
                //    Size = "68x128x85",
                //    Model = "INV,SV001IE5-2",
                //    Article = "6620000200",
                //    Amperage = 0.8F
                //},
                //new FrequencyConverter
                //{
                //    SeriesId = 1,
                //    PowerId = 2,
                //    PhasesId = 2,
                //    idDescriptions = 1,
                //    MoneyCost = 220.2F,
                //    Size = "68x128x85",
                //    Model = "INV,SV002IE5-2",
                //    Article = "6620000200",
                //    Amperage = 1.6F
                //},
                //new FrequencyConverter
                //{
                //    SeriesId = 1,
                //    PowerId = 3,
                //    PhasesId = 2,
                //    idDescriptions = 1,
                //    MoneyCost = 231.21F,
                //    Size = "68x128x85",
                //    Model = "INV,SV004IE5-2",
                //    Article = "6620000600",
                //    Amperage = 3
                //});
                // new FrequencyConverter
                // {
                //     SeriesId = 2,
                //     PowerId = 3,
                //     PhasesId = 1,
                //     idDescriptions = 1,
                //     MoneyCost = 227.34F,
                //     Size = "79x143x143",
                //     Model = "SV004IC5-1",
                //     Article = "6600000100",
                //     Amperage = 2.5F
                // },
                // new FrequencyConverter
                // {
                //     SeriesId = 2,
                //     PowerId = 4,
                //     PhasesId = 1,
                //     idDescriptions = 1,
                //     MoneyCost = 261.23F,
                //     Size = "79x143x143",
                //     Model = "SV008IC5-1",
                //     Article = "6600000200",
                //     Amperage = 5F
                // },
                // new FrequencyConverter
                // {
                //     SeriesId = 2,
                //     PowerId = 5,
                //     PhasesId = 1,
                //     idDescriptions = 1,
                //     MoneyCost = 313.27F,
                //     Size = "156x143x143",
                //     Model = "SV0015IC5-1",
                //     Article = "6600000300",
                //     Amperage = 8F
                // }, 
                // new FrequencyConverter
                // {
                //     SeriesId = 2,
                //     PowerId = 6,
                //     PhasesId = 1,
                //     idDescriptions = 1,
                //     MoneyCost = 387.78F,
                //     Size = "156x143x143",
                //     Model = "SV0022IC5-1",
                //     Article = "6600000400",
                //     Amperage = 12F
                // },
                //new FrequencyConverter
                //{
                //    SeriesId = 1,
                //    PowerId = 3,
                //    PhasesId = 2,
                //    idDescriptions = 1,
                //    MoneyCost = 234.3F,
                //    Size = "79x143x143",
                //    Model = "SV004IC5-1F",
                //    Article = "6600000500",
                //    Amperage = 2.5F
                //},
                //new FrequencyConverter
                //{
                //    SeriesId = 1,
                //    PowerId = 4,
                //    PhasesId = 2,
                //    idDescriptions = 1,
                //    MoneyCost = 267.1F,
                //    Size = "79x143x143",
                //    Model = "SV008IC5-1F",
                //    Article = "6600000600",
                //    Amperage = 5F
                //},
                //new FrequencyConverter
                //{
                //    SeriesId = 1,
                //    PowerId = 5,
                //    PhasesId = 2,
                //    idDescriptions = 1,
                //    MoneyCost = 330F,
                //    Size = "156x143x143",
                //    Model = "SV0015IC5-1F",
                //    Article = "6600000700",
                //    Amperage = 8F
                //},
                //new FrequencyConverter
                //{
                //    SeriesId = 1,
                //    PowerId = 6,
                //    PhasesId = 2,
                //    idDescriptions = 1,
                //    MoneyCost = 288.41F,
                //    Size = "156x143x143",
                //    Model = "SV0022IC5-1F",
                //    Article = "6600000800",
                //    Amperage = 12F
                //});
                //context.SaveChanges();

                #endregion
                #region InitFilterName
                //context.FilterNames.AddRange(
                //    new FilterName
                //    {
                //        Name = "Series"
                //    },
                //     new FilterName
                //     {
                //         Name = "Power"
                //     },
                //      new FilterName
                //      {
                //          Name = "Phases"
                //      },
                //       new FilterName
                //       {
                //           Name = "Price"
                //       });
                //context.SaveChanges();
                #endregion

                #region InitFilterValue
                //context.FilterValues.AddRange(
                //     new FilterValue { Name = "Starvert iE5" },
                //    new FilterValue { Name = "Starvert iC5" },
                //    new FilterValue { Name = "Starvert iG5A" },
                //    new FilterValue { Name = "S100" },
                //    new FilterValue { Name = "Starvert iS7" },
                //    new FilterValue { Name = "Starvert iV5" },
                //    new FilterValue { Name = "Starvert iP5A" },
                //    new FilterValue { Name = "H100" },

                //    new FilterValue { Name = "1" },
                //    new FilterValue { Name = "3" },

                //    new FilterValue { Name = "0.1" },
                //    new FilterValue { Name = "0.2" },
                //    new FilterValue { Name = "0.4" },
                //    new FilterValue { Name = "0.75" },
                //    new FilterValue { Name = "1.5" },
                //    new FilterValue { Name = "2.2" },
                //    new FilterValue { Name = "3.7" },
                //    new FilterValue { Name = "4" },
                //    new FilterValue { Name = "5.5" },
                //    new FilterValue { Name = "7,5" },
                //    new FilterValue { Name = "11" },
                //    new FilterValue { Name = "15" },
                //    new FilterValue { Name = "18.5" },
                //    new FilterValue { Name = "22" },
                //    new FilterValue { Name = "30" },
                //    new FilterValue { Name = "37" },
                //    new FilterValue { Name = "45" },
                //    new FilterValue { Name = "55" },
                //    new FilterValue { Name = "75" });
                //context.SaveChanges();
                //#endregion

                //#region InitFilterNameGroups

                //FilterNameGroup[] filterNameGroups =
                //{
                //    new FilterNameGroup { FilterNameId = 1, FilterValueId=5 },
                //    new FilterNameGroup { FilterNameId = 1, FilterValueId=6 },
                //    new FilterNameGroup { FilterNameId = 1, FilterValueId=7 },
                //    new FilterNameGroup { FilterNameId = 1, FilterValueId=8 },
                //    new FilterNameGroup { FilterNameId = 2, FilterValueId=1 },
                //    new FilterNameGroup { FilterNameId = 2, FilterValueId=2 },
                //    new FilterNameGroup { FilterNameId = 2, FilterValueId=3 },
                //    new FilterNameGroup { FilterNameId = 2, FilterValueId=4 },
                //};
                //context.FilterNameGroups.AddRange(filterNameGroups);
                #endregion

                #region InitFilter
                //context.Filters.AddRange(

                //    //series 1 power 2 phases 3
                //    new Filter { FilterNameId = 1, FilterValueId = 1, ProductId = 1 },
                //     new Filter { FilterNameId = 1, FilterValueId = 1, ProductId = 2 },
                //      new Filter { FilterNameId = 1, FilterValueId = 1, ProductId = 3 },
                //       new Filter { FilterNameId = 1, FilterValueId = 1, ProductId = 4 },
                //        new Filter { FilterNameId = 1, FilterValueId = 1, ProductId = 5 },
                //         new Filter { FilterNameId = 1, FilterValueId = 1, ProductId = 6 },

                //    new Filter { FilterNameId = 1, FilterValueId = 2, ProductId = 7 },
                //     new Filter { FilterNameId = 1, FilterValueId = 2, ProductId = 8 },
                //      new Filter { FilterNameId = 1, FilterValueId = 2, ProductId = 9 },
                //       new Filter { FilterNameId = 1, FilterValueId = 2, ProductId = 10 },
                //        new Filter { FilterNameId = 1, FilterValueId = 2, ProductId = 11 },
                //         new Filter { FilterNameId = 1, FilterValueId = 2, ProductId = 12 },
                //          new Filter { FilterNameId = 1, FilterValueId = 2, ProductId = 13 },
                //           new Filter { FilterNameId = 1, FilterValueId = 2, ProductId = 14 },

                //    new Filter { FilterNameId = 3, FilterValueId = 9, ProductId = 1 },
                //     new Filter { FilterNameId = 3, FilterValueId = 9, ProductId = 2 },
                //      new Filter { FilterNameId = 3, FilterValueId = 9, ProductId = 3 },
                //       new Filter { FilterNameId = 3, FilterValueId = 9, ProductId = 7 },
                //        new Filter { FilterNameId = 3, FilterValueId = 9, ProductId = 8 },
                //         new Filter { FilterNameId = 3, FilterValueId = 9, ProductId = 9 }, 
                //          new Filter { FilterNameId = 3, FilterValueId = 9, ProductId = 10 },
                //           new Filter { FilterNameId = 3, FilterValueId = 9, ProductId = 19 },
                //            new Filter { FilterNameId = 3, FilterValueId = 9, ProductId = 20 },
                //             new Filter { FilterNameId = 3, FilterValueId = 9, ProductId = 21 },




                //    new Filter { FilterNameId = 3, FilterValueId = 10, ProductId = 4 },
                //     new Filter { FilterNameId = 3, FilterValueId = 10, ProductId = 5 },
                //      new Filter { FilterNameId = 3, FilterValueId = 10, ProductId = 6 },
                //       new Filter { FilterNameId = 3, FilterValueId = 10, ProductId = 11 },
                //        new Filter { FilterNameId = 3, FilterValueId = 10, ProductId = 12 },
                //         new Filter { FilterNameId = 3, FilterValueId = 10, ProductId = 13 }, 
                //          new Filter { FilterNameId = 3, FilterValueId = 10, ProductId = 14 },
                //           new Filter { FilterNameId = 3, FilterValueId = 10, ProductId = 22 }
                //);
                //context.SaveChanges();
                #endregion

                //context.FilterNameGroups.AddRange(
                //    new FilterNameGroup
                //    {
                //        FilterNameId = 1,
                //        FilterValueId = 1
                //    },
                //    new FilterNameGroup
                //    {
                //        FilterNameId = 1,
                //        FilterValueId = 2
                //    },
                //    new FilterNameGroup
                //    {
                //        FilterNameId = 1,
                //        FilterValueId = 3
                //    },
                //    new FilterNameGroup
                //    {
                //        FilterNameId = 1,
                //        FilterValueId = 4
                //    },
                //    new FilterNameGroup
                //    {
                //        FilterNameId = 1,
                //        FilterValueId = 5
                //    },
                //    new FilterNameGroup
                //    {
                //        FilterNameId = 1,
                //        FilterValueId = 6
                //    },
                //    new FilterNameGroup
                //    {
                //        FilterNameId = 1,
                //        FilterValueId = 7
                //    },
                //    new FilterNameGroup
                //    {
                //        FilterNameId = 1,
                //        FilterValueId = 8
                //    },
                //    new FilterNameGroup
                //    {
                //        FilterNameId = 2,
                //        FilterValueId = 11
                //    },
                //     new FilterNameGroup
                //     {
                //         FilterNameId = 2,
                //         FilterValueId = 12
                //     },
                //      new FilterNameGroup
                //      {
                //          FilterNameId = 2,
                //          FilterValueId = 13
                //      },
                //       new FilterNameGroup
                //       {
                //           FilterNameId = 2,
                //           FilterValueId = 14
                //       },
                //        new FilterNameGroup
                //        {
                //            FilterNameId = 2,
                //            FilterValueId = 15
                //        },
                //         new FilterNameGroup
                //         {
                //             FilterNameId = 2,
                //             FilterValueId = 16
                //         },
                //          new FilterNameGroup
                //          {
                //              FilterNameId = 2,
                //              FilterValueId = 17
                //          },
                //           new FilterNameGroup
                //           {
                //               FilterNameId = 2,
                //               FilterValueId = 18
                //           },
                //            new FilterNameGroup
                //            {
                //                FilterNameId = 2,
                //                FilterValueId = 19
                //            },
                //             new FilterNameGroup
                //             {
                //                 FilterNameId = 2,
                //                 FilterValueId = 20
                //             },
                //              new FilterNameGroup
                //              {
                //                  FilterNameId = 2,
                //                  FilterValueId = 21
                //              },
                //               new FilterNameGroup
                //               {
                //                   FilterNameId = 2,
                //                   FilterValueId = 22
                //               },
                //                new FilterNameGroup
                //                {
                //                    FilterNameId = 2,
                //                    FilterValueId = 23
                //                },
                //                 new FilterNameGroup
                //                 {
                //                     FilterNameId = 2,
                //                     FilterValueId = 11
                //                 },
                //                  new FilterNameGroup
                //                  {
                //                      FilterNameId = 2,
                //                      FilterValueId = 24
                //                  },
                //                   new FilterNameGroup
                //                   {
                //                       FilterNameId = 2,
                //                       FilterValueId = 25
                //                   },
                //                    new FilterNameGroup
                //                    {
                //                        FilterNameId = 2,
                //                        FilterValueId = 26
                //                    },
                //                     new FilterNameGroup
                //                     {
                //                         FilterNameId = 2,
                //                         FilterValueId = 27
                //                     },
                //                      new FilterNameGroup
                //                      {
                //                          FilterNameId = 2,
                //                          FilterValueId = 28
                //                      },
                //                       new FilterNameGroup
                //                       {
                //                          FilterNameId = 2,
                //                          FilterValueId = 29
                //                       },
                //        new FilterNameGroup
                //        {
                //            FilterNameId = 3,
                //            FilterValueId = 9
                //        },
                //        new FilterNameGroup
                //        {
                //            FilterNameId = 3,
                //            FilterValueId = 10
                //        });
                //context.SaveChanges();


                //context.Filters.AddRange(
                //    new Filter
                //    {
                //        ProductId = 1,
                //        FilterNameId = 1,
                //        FilterValueId = 1
                //    },
                //     new Filter
                //     {
                //         ProductId = 2,
                //         FilterNameId = 1,
                //         FilterValueId = 1
                //     }, new Filter
                //     {
                //         ProductId = 3,
                //         FilterNameId = 1,
                //         FilterValueId = 1
                //     }, new Filter
                //     {
                //         ProductId = 5,
                //         FilterNameId = 1,
                //         FilterValueId = 1
                //     }, new Filter
                //     {
                //         ProductId = 6,
                //         FilterNameId = 1,
                //         FilterValueId = 1
                //     }, new Filter
                //     {
                //         ProductId = 7,
                //         FilterNameId = 1,
                //         FilterValueId = 1
                //     }, new Filter
                //     {
                //         ProductId = 8,
                //         FilterNameId = 1,
                //         FilterValueId = 2
                //     }, new Filter
                //     {
                //         ProductId = 9,
                //         FilterNameId = 1,
                //         FilterValueId = 2
                //     }, new Filter
                //     {
                //         ProductId = 10,
                //         FilterNameId = 1,
                //         FilterValueId = 2
                //     }, new Filter
                //     {
                //         ProductId = 11,
                //         FilterNameId = 1,
                //         FilterValueId = 2
                //     }, new Filter
                //     {
                //         ProductId = 12,
                //         FilterNameId = 1,
                //         FilterValueId = 2
                //     }, new Filter
                //     {
                //         ProductId = 13,
                //         FilterNameId = 1,
                //         FilterValueId = 2
                //     }, new Filter
                //     {
                //         ProductId = 14,
                //         FilterNameId = 1,
                //         FilterValueId = 2
                //     }, new Filter
                //     {
                //         ProductId = 15,
                //         FilterNameId = 1,
                //         FilterValueId = 2
                //     });
                //context.SaveChanges();
                //context.Filters.AddRange(
                //    new Filter
                //    {
                //        ProductId = 16,
                //        FilterNameId = 1,
                //        FilterValueId = 3
                //    },
                //     new Filter
                //     {
                //         ProductId = 17,
                //         FilterNameId = 1,
                //         FilterValueId = 3
                //     },
                //      new Filter
                //      {
                //          ProductId = 19,
                //          FilterNameId = 1,
                //          FilterValueId = 3
                //      }, new Filter
                //      {
                //          ProductId = 20,
                //          FilterNameId = 1,
                //          FilterValueId = 3
                //      }, new Filter
                //      {
                //          ProductId = 21,
                //          FilterNameId = 1,
                //          FilterValueId = 3
                //      }, new Filter
                //      {
                //          ProductId = 22,
                //          FilterNameId = 1,
                //          FilterValueId = 3
                //      }, new Filter
                //      { 
                //          ProductId = 23,
                //          FilterNameId = 1,
                //          FilterValueId = 3
                //      }, new Filter
                //      {
                //          ProductId = 24,
                //          FilterNameId = 1,
                //          FilterValueId = 3
                //      }, new Filter
                //      {
                //          ProductId = 25,
                //          FilterNameId = 1,
                //          FilterValueId = 3
                //      }, new Filter
                //      {
                //          ProductId = 26,
                //          FilterNameId = 1,
                //          FilterValueId = 3
                //      }, new Filter
                //      {
                //          ProductId = 28,
                //          FilterNameId = 1,
                //          FilterValueId = 3
                //      }, new Filter
                //      {
                //          ProductId = 29,
                //          FilterNameId = 1,
                //          FilterValueId = 3
                //      }, new Filter
                //      {
                //          ProductId = 30,
                //          FilterNameId = 1,
                //          FilterValueId = 3
                //      }, new Filter
                //      {
                //          ProductId = 31,
                //          FilterNameId = 1,
                //          FilterValueId = 3
                //      });

                //context.Filters.AddRange(
                //    new Filter
                //    {
                //        ProductId = 54,
                //        FilterNameId = 1,
                //        FilterValueId = 5
                //    },
                //     new Filter
                //     {
                //         ProductId = 55,
                //         FilterNameId = 1,
                //         FilterValueId = 5
                //     },
                //      new Filter
                //      {
                //          ProductId = 56,
                //          FilterNameId = 1,
                //          FilterValueId = 5
                //      },
                //       new Filter
                //       {
                //           ProductId = 57,
                //           FilterNameId = 1,
                //           FilterValueId = 5
                //       },
                //        new Filter
                //        {
                //            ProductId = 58,
                //            FilterNameId = 1,
                //            FilterValueId = 5
                //        },
                //         new Filter
                //         {
                //             ProductId = 59,
                //             FilterNameId = 1,
                //             FilterValueId = 5
                //         },
                //          new Filter
                //          {
                //              ProductId = 60,
                //              FilterNameId = 1,
                //              FilterValueId = 5
                //          },
                //           new Filter
                //           {
                //               ProductId = 61,
                //               FilterNameId = 1,
                //               FilterValueId = 5
                //           },
                //            new Filter
                //            {
                //                ProductId = 62,
                //                FilterNameId = 1,
                //                FilterValueId = 5
                //            },
                //                 new Filter
                //                 {
                //                     ProductId = 63,
                //                     FilterNameId = 1,
                //                     FilterValueId = 5
                //                 },
                //                new Filter
                //                {
                //                    ProductId = 64,
                //                    FilterNameId = 1,
                //                    FilterValueId = 5
                //                },
                //                new Filter
                //                {
                //                    ProductId = 65,
                //                    FilterNameId = 1,
                //                    FilterValueId = 5
                //                },
                //                new Filter
                //                {
                //                    ProductId = 66,
                //                    FilterNameId = 1,
                //                    FilterValueId = 5
                //                },
                //                new Filter
                //                {
                //                    ProductId = 67,
                //                    FilterNameId = 1,
                //                    FilterValueId = 5
                //                },
                //                new Filter
                //                {
                //                    ProductId = 68,
                //                    FilterNameId = 1,
                //                    FilterValueId = 5
                //                }   
                //   );
                //context.Filters.AddRange(
                //    new Filter
                //    {
                //        ProductId = 69,
                //        FilterNameId = 1,
                //        FilterValueId = 6
                //    },
                //    new Filter
                //    {
                //        ProductId = 70,
                //        FilterNameId = 1,
                //        FilterValueId = 6
                //    },
                //    new Filter
                //    {
                //        ProductId = 71,
                //        FilterNameId = 1,
                //        FilterValueId = 6
                //    },
                //    new Filter
                //    {
                //        ProductId = 72,
                //        FilterNameId = 1,
                //        FilterValueId = 6
                //    },
                //    new Filter
                //    {
                //        ProductId = 73,
                //        FilterNameId = 1,
                //        FilterValueId = 6
                //    },
                //    new Filter
                //    {
                //        ProductId = 74,
                //        FilterNameId = 1,
                //        FilterValueId = 6
                //    },
                //    new Filter
                //    {
                //        ProductId = 75,
                //        FilterNameId = 1,
                //        FilterValueId = 6
                //    },
                //    new Filter
                //    {
                //        ProductId = 76,
                //        FilterNameId = 1,
                //        FilterValueId = 6
                //    },
                //     new Filter
                //     {
                //         ProductId = 77,
                //         FilterNameId = 1,
                //         FilterValueId = 6

                //     },
                //      new Filter
                //      {
                //          ProductId = 79,
                //          FilterNameId = 1,
                //          FilterValueId = 6
                //      },
                //       new Filter
                //       {
                //           ProductId = 80,
                //           FilterNameId = 1,
                //           FilterValueId = 6
                //       });

                //context.Filters.AddRange(
                //       new Filter
                //       {
                //           ProductId = 94,
                //           FilterNameId = 1,
                //           FilterValueId =8
                //       },
                //        new Filter
                //        {
                //            ProductId = 95,
                //            FilterNameId = 1,
                //            FilterValueId = 8
                //        },
                //         new Filter
                //         {
                //             ProductId = 96,
                //             FilterNameId = 1,
                //             FilterValueId = 8
                //         },
                //          new Filter
                //          {
                //              ProductId = 97,
                //              FilterNameId = 1,
                //              FilterValueId = 8
                //          },
                //           new Filter
                //           {
                //               ProductId = 98,
                //               FilterNameId = 1,
                //               FilterValueId = 8
                //           },
                //            new Filter
                //            {
                //                ProductId = 99,
                //                FilterNameId = 1,
                //                FilterValueId = 8
                //            },
                //                 new Filter
                //                 {
                //                     ProductId = 100,
                //                     FilterNameId = 1,
                //                     FilterValueId = 8
                //                 },
                //                new Filter
                //                {
                //                    ProductId = 101,
                //                    FilterNameId = 1,
                //                    FilterValueId = 8
                //                },
                //                new Filter
                //                {
                //                    ProductId = 102,
                //                    FilterNameId = 1,
                //                    FilterValueId = 8
                //                },
                //                new Filter
                //                {
                //                    ProductId = 104,
                //                    FilterNameId = 1,
                //                    FilterValueId = 8
                //                },
                //                    new Filter
                //                    {
                //                        ProductId = 105,
                //                        FilterNameId = 1,
                //                        FilterValueId = 8
                //                    }
                //         );
                //context.Filters.AddRange(
                //      new Filter
                //      {
                //          ProductId = 1,
                //          FilterNameId = 3,
                //          FilterValueId = 9
                //      },
                //       new Filter
                //       {
                //           ProductId = 2,
                //           FilterNameId = 3,
                //           FilterValueId = 9
                //       },
                //        new Filter
                //        {
                //            ProductId = 3,
                //            FilterNameId = 3,
                //            FilterValueId = 9
                //        },
                //         new Filter
                //         {
                //             ProductId = 8,
                //             FilterNameId = 3,
                //             FilterValueId = 9
                //         },
                //          new Filter
                //          {
                //              ProductId = 9,
                //              FilterNameId = 3,
                //              FilterValueId = 9
                //          },
                //           new Filter
                //           {
                //               ProductId = 10,
                //               FilterNameId = 3,
                //               FilterValueId = 9
                //           },
                //            new Filter
                //            {
                //                ProductId = 11,
                //                FilterNameId = 3,
                //                FilterValueId = 9
                //            },
                //             new Filter
                //             {
                //                 ProductId = 12,
                //                 FilterNameId = 3,
                //                 FilterValueId = 9
                //             },
                //              new Filter
                //              {
                //                  ProductId = 13,
                //                  FilterNameId = 3,
                //                  FilterValueId = 9
                //              },
                //               new Filter
                //               {
                //                   ProductId = 14,
                //                   FilterNameId = 3,
                //                   FilterValueId = 9
                //               },
                //                new Filter
                //                {
                //                    ProductId = 15,
                //                    FilterNameId = 3,
                //                    FilterValueId = 9
                //                },
                //                 new Filter
                //                 {
                //                     ProductId = 16,
                //                     FilterNameId = 3,
                //                     FilterValueId = 9
                //                 },
                //                  new Filter
                //                  {
                //                      ProductId = 17,
                //                      FilterNameId = 3,
                //                      FilterValueId = 9
                //                  },
                //                   new Filter
                //                   {
                //                       ProductId = 19,
                //                       FilterNameId = 3,
                //                       FilterValueId = 9
                //                   });

                //var seed = context.FrequencyConverters.Where(s=>s.PhasesId == 2).ToList();
                // foreach (var item in seed)
                // {
                //     context.Filters.AddRange(new Filter
                //     {
                //         ProductId = item.Id,
                //         FilterNameId = 3,
                //         FilterValueId = 10
                //     });
                // }
                //var seed = context.FrequencyConverters.Where(s => s.PowerId == 19).ToList();
                //foreach (var item in seed)
                //{
                //    context.Filters.AddRange(new Filter
                //    {
                //        ProductId = item.Id,
                //        FilterNameId = 2,
                //        FilterValueId = 29
                //    });

                //}
                //context.SaveChanges();
                
            }
        }
    }
}
