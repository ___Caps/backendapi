﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ngShopAPI.Models
{
    public class FValueViewModel
    {
        public int? Id { get; set; }
        public string Name { get; set; }
    }
    public class FNameViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<FValueViewModel> Children { get; set; }
    }
    public class ProductViewModel
    {
        public int Id { get; set; }
        public string LSeries { get; set; }
        public float LPower { get; set; }
        public int LPhases { get; set; }
        public float LMoney { get; set; }
        public string LDescription { get; set; }
        public string LImagePath { get; set; }
    }

    public class UserViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Role { get; set; }
    }
    public class HomeViewModel
    {
        public List<FNameViewModel> Filters { get; set; }
        public List<ProductViewModel> Products { get; set; }
    }
}
