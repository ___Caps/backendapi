﻿using DAL;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ngShopAPI.Models
{
    public class EFContext : IdentityDbContext<DbUser>
    {
        //protected override void OnModelCreating(ModelBuilder builder)
        //{
        //    //builder.Entity<FilterName>().HasKey(ca => new { ca.Id });

        //    base.OnModelCreating(builder);
        //}

        public EFContext(DbContextOptions<EFContext> options)
              : base(options)
        {
            Database.EnsureCreated();
        }

        public DbSet<FrequencyConverter> FrequencyConverters { get; set; }
        public DbSet<Series> Series { get; set; }
        public DbSet<Description> Description { get; set; }
        public DbSet<Power> Power { get; set; }
        public DbSet<CountPhases> CountPhases { get; set; }

        public DbSet<UserInfo> UserInfo { get; set; }
        public DbSet<UserRolesInfo> UserRolesInfo { get; set; }

        
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderProduct> OrderProducts { get; set; }
        //-
        public DbSet<Filter> Filters { get; set; }
        public DbSet<FilterName> FilterNames { get; set; }
        public DbSet<vFilterNameGroup> VFilterNameGroups { get; set; }
        public DbSet<FilterNameGroup> FilterNameGroups { get; set; }
        public DbSet<FilterValue> FilterValues { get; set; }
    }
}
