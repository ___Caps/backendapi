﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DAL
{
    public class Series
    {
        [Key]
        public int SeriesId { get; set; }

        [Required]
        public string SeriesName { get; set; }
        
        public string ImagePath { get; set; }

        public ICollection<FrequencyConverter> FrequencyConverter { get; set; }
    }
}
