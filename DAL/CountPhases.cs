﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DAL
{
    public class CountPhases
    {
        [Key]
        public int PhasesId { get; set; }

        [Required]
        public int PhasesCount { get; set; }

        public ICollection<FrequencyConverter> FrequencyConverter { get; set; }
    }
}
