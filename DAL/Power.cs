﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DAL
{
    public class Power
    {
        [Key]
        public int PowerId { get; set; }

        [Required]
        public float CountPower { get; set; }

        public ICollection<FrequencyConverter> FrequencyConverter { get; set; }
    }
}