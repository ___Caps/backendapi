﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DAL
{
    public class OrderProduct
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("Order")]
        public int OrderId { get; set; }
        public virtual Order Order { get; set; }

        public int? Count { get; set; }
        [ForeignKey("FrequencyConverter")]
        public int FrequencyConverterId { get; set; }
        public virtual FrequencyConverter FrequencyConverter { get; set; }
    }
}
