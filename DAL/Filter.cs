﻿using DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace DAL
{
    public class Filter
    {
        [Key]
        public int Id { get; set; }
        [ForeignKey("FilterNameOf")]
        public int FilterNameId { get; set; }
        public virtual FilterName FilterNameOf { get; set; }

        [ForeignKey("FilterValueOf")]
        public int FilterValueId { get; set; }
        public virtual FilterValue FilterValueOf { get; set; }


        [ForeignKey("ProductOf")]
        public int ProductId { get; set; }
        public virtual FrequencyConverter ProductOf { get; set; }
    }
}