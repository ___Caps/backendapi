﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DAL
{
    public class FrequencyConverter
    {
        [Key, Column(Order = 0)]
        public int Id { get; set; }

        [ForeignKey("Series")]
        public int SeriesId { get; set; }

        [ForeignKey("Power")]
        public int PowerId { get; set; }

        [ForeignKey("CountPhases")]
        public int PhasesId { get; set; }
        [ForeignKey("Description")]
        public int idDescriptions { get; set; }


        [Required]
        public float MoneyCost { get; set; }

        //DLC
        public string Size { get; set; }
        public string Model { get; set; }
        public string Article { get; set; }
        public float ? Amperage { get; set; }
        //
        public virtual Series Series { get; set; }
        public virtual Power Power { get; set; }
        public virtual CountPhases CountPhases { get; set; }
        public virtual Description Description { get; set; }


        public virtual ICollection<Filter> Filtres { get; set; }
        public virtual ICollection<OrderProduct> OrderProducts { get; set; }
    }
}
