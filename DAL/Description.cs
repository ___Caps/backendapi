﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DAL
{
    public class Description
    {
        [Key]
        public int DescriptionId { get; set; }

        [Required]
        public string Descriptions { get; set; }

        public string NameSeries { get; set; }

        public ICollection<FrequencyConverter> FrequencyConverter { get; set; }
    }
}
