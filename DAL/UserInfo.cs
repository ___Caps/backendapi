﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DAL
{
    public class UserInfo
    {
        [Key]
        public string Id { get; set; }
        [Required, StringLength(maximumLength:50)]
        public string Name { get; set; }
        [Required, StringLength(maximumLength: 100)]
        public string Email { get; set; }
        [Required, StringLength(maximumLength: 50)]
        public string Password { get; set; }
        [StringLength(maximumLength: 500)]
        public string userDetails { get; set; }
        public DateTime ? RegisterTime { get; set; }
        public bool ? isOnline { get; set; }

        [ForeignKey("Role")]
        public int? RoleId { get; set; }
        public UserRolesInfo Role { get; set; }

        public virtual ICollection<Order> Order { get; set; }


    }
}
