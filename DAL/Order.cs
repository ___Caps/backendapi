﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DAL
{
    public class Order
    {
        [Key]
        public int Id { get; set; }
        [ForeignKey("UserInfoOf")]
        public string UserId { get; set; }
        public virtual UserInfo UserInfoOf { get; set;}
        public virtual ICollection<OrderProduct> OrderProducts { get; set; }
    }
}
